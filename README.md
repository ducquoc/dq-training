# DQ-Training #

General samples for internal training. 
(see also: https://github.com/ducquoc/fresher-training)

### Rationale ###

* Extraction from some of my projects and utils, for training/POC purpose.
* May refer to some GitHub public repos of mine (too lazy to copy here).

### How to set up and run ###

See [Markdown](https://bitbucket.org/tutorials/markdowndemo) files (README.md, README.markdown) or README.txt in each project.

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Feedback and comments ###

* mailto: ducquoc DOT vn AT gmail DOT com
* Any member of: https://bitbucket.org/dqonline or https://bitbucket.org/dqprotected
