-- sqlplus sys/oracle@host:port/xe
DROP USER data_test;
CREATE USER data_test IDENTIFIED BY oracle;
GRANT CONNECT,RESOURCE TO data_test;

ALTER USER data_test IDENTIFIED BY changeit;
GRANT DBA TO data_test;

CONNECT data_test/changeit;
DROP TABLE "COMPANY"; /* schema data_test , ignore not exist -942 */
CREATE TABLE "COMPANY" (
    "ID" NUMBER(19,0),
    "NAME" VARCHAR2(255),
    "AGE" NUMBER(9,0),
    "ADDRESS" VARCHAR2(4000),
    "SALARY" NUMBER(19,0),
    CONSTRAINT COMPANY_PK PRIMARY KEY (ID)
    );
DROP SEQUENCE "COMPANY_SEQ"; /* ignore not exist -2289 */
CREATE SEQUENCE  "COMPANY_SEQ"  MINVALUE 1 INCREMENT BY 1 START WITH 1;
DISCONNECT;

