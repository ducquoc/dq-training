-- psql -h localhost -p 5432 -U data_test -d data_test_db
SET SCHEMA 'data_test_schema';

DROP TABLE IF EXISTS company;
CREATE TABLE IF NOT EXISTS company(
    id bigint,
    name text,
    age integer,
    address character(255),
    salary real,
    CONSTRAINT company_pkey PRIMARY KEY (id)
);
DROP SEQUENCE IF EXISTS company_seq;
CREATE SEQUENCE IF NOT EXISTS company_seq INCREMENT 1 START 1  MINVALUE 1;

DELETE FROM company WHERE salary=4444 OR salary=5555;
INSERT INTO company(ID,NAME,AGE,ADDRESS,SALARY)
  VALUES(nextval('company_seq'),'psql',22,'CLI client',4444);
INSERT INTO company(ID,NAME,AGE,ADDRESS,SALARY)
  VALUES(nextval('company_seq'),'pgAdmin4',23,'GUI client',5555);

ALTER TABLE company DROP COLUMN IF EXISTS is_growing;
ALTER TABLE company ADD is_growing BOOLEAN /* DEFAULT true */;
UPDATE company SET is_growing=true WHERE is_growing IS NULL;
-- exit; /* \q */

