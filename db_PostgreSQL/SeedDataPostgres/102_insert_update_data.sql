-- psql -h localhost -p 5432 -U data_test -d data_test_db
SET SCHEMA 'data_test_schema'; /* default schema 'public', switch explicitly - no need prefix schema */
DELETE FROM company WHERE salary=4444 OR salary=5555;
INSERT INTO company(ID,NAME,AGE,ADDRESS,SALARY)
  VALUES(nextval('company_seq'),'psql',22,'command-line client',4444);
INSERT INTO company(ID,NAME,AGE,ADDRESS,SALARY)
  VALUES(nextval('company_seq'),'pgAdmin4',23,'GUI client',5555);
UPDATE COMPANY SET address='CLI client' WHERE salary=4444;
-- exit; /* \q */

