-- psql -h localhost -p 5432 -U postgres -d postgres
DROP USER data_test;
CREATE USER data_test WITH PASSWORD 'postgres';
DROP DATABASE data_test_db; /* default schema 'public' */
CREATE DATABASE data_test_db WITH OWNER=data_test ENCODING='UTF8';

ALTER USER data_test WITH PASSWORD 'changeit';
GRANT ALL PRIVILEGES ON DATABASE data_test_db TO data_test;
--GRANT ALL PRIVILEGES ON DATABASE data_test_db TO postgres;
-- exit; /* \q */

-- psql -h localhost -p 5432 -U data_test -d data_test_db
CREATE SCHEMA IF NOT EXISTS data_test_schema AUTHORIZATION data_test;
GRANT ALL ON SCHEMA data_test_schema TO data_test; /* already AUTHORIZATION */
SET SCHEMA 'data_test_schema'; /* default schema 'public', switch explicitly - no need prefix schema */
DROP TABLE company;
CREATE TABLE company(
    id bigint,
    name text,
    age integer,
    address character(255),
    salary real,
    CONSTRAINT company_pkey PRIMARY KEY (id)
);
DROP SEQUENCE company_seq;
CREATE SEQUENCE company_seq INCREMENT 1 START 1  MINVALUE 1;
-- exit; /* \q */

