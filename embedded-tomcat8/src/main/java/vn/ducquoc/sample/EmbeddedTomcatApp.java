package vn.ducquoc.sample;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.catalina.Context;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.startup.Tomcat;

public class EmbeddedTomcatApp {

    public static void main(String[] args) throws LifecycleException, InterruptedException, ServletException {
        Tomcat tomcat = new Tomcat();
        tomcat.setPort(8080);

        Context ctx = tomcat.addContext("/", new File(".").getAbsolutePath());

        Tomcat.addServlet(ctx, "helloServlet", new HttpServlet() {
            //private static final long serialVersionUID = 1L;

            @Override
            protected void service(HttpServletRequest request, HttpServletResponse response)
                    throws ServletException, IOException {
                response.setCharacterEncoding("UTF-8");
                response.setContentType("text/plain");
                try (Writer writer = response.getWriter()) {
                    writer.write("Hello World");
                    writer.flush();
                }
            }
        });
        ctx.addServletMappingDecoded("/*", "helloServlet");

        // 2nd servlet
        Tomcat.addServlet(ctx, "downloadCsvServlet", new HttpServlet() {
            //private static final long serialVersionUID = 1L;
            @Override
            protected void service(HttpServletRequest request, HttpServletResponse response)
                    throws ServletException, IOException {
                response.setCharacterEncoding("UTF-8");
                response.setContentType("text/csv");
                String fileName = "hw.csv";
                response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
                try (Writer writer = response.getWriter()) { //Java 7 try-with-resources
                    writer.write("first,last" + System.getProperty("line.separator"));
                    writer.write("Hello,World" + System.getProperty("line.separator"));
                    writer.flush();
                }
            }
        });
        ctx.addServletMappingDecoded("/download", "downloadCsv");

        tomcat.start(); //LifecycleException
        tomcat.getServer().await();
    }

}
