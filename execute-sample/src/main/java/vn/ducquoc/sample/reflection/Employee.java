package vn.ducquoc.sample.reflection;

/**
 * Created by ducquoc on 3/16/2018.
 */
public class Employee extends Person {

    //protected info of an employee (no Getter/Setter)
    protected int bodyMassIndex = 19;

    public String getFirstName() {
        return "Too";
    }

    public String getLastName() {
        return "Piece";
    }

    public int getVacationDays() {
        return 2;
    }

}
