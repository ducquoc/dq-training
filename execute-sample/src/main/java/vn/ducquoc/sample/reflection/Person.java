package vn.ducquoc.sample.reflection;

/**
 * Created by ducquoc on 3/16/2018.
 */
public class Person {

    public String getFirstName() {
        return "Italian";
    }

    public String getLastName() {
        return "Malta";
    }

}
