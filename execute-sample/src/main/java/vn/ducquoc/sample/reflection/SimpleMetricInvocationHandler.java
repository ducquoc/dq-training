package vn.ducquoc.sample.reflection;

import vn.ducquoc.sample.reflection.annotation.DurationMetric;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ducquoc on 3/18/2018.
 */
public class SimpleMetricInvocationHandler implements InvocationHandler {

    private static java.util.logging.Logger LOGGER = java.util.logging.Logger.getGlobal();

    private final Map<String, Method> metricRegistryMap = new HashMap<>();

    private Object target;

    public SimpleMetricInvocationHandler(Object target) {
        this.target = target;

        for (Method method : target.getClass().getDeclaredMethods()) {
            this.metricRegistryMap.put(method.getName(), method);
        }
    }

    /**
     * @override
     */
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Method proxiedMethod = metricRegistryMap.get(method.getName());
        long start = System.nanoTime();
        Object result = proxiedMethod.invoke(target, args);
        long elapsed = System.nanoTime() - start;

        if (proxiedMethod.getAnnotation(DurationMetric.class) != null) { //isAnnotationPresent
            LOGGER.info("Executing " + method.getName() + " took " + elapsed / 1e6 + " ms");
        }

        return result;
    }

}
