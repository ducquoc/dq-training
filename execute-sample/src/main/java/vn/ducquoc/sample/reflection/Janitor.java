package vn.ducquoc.sample.reflection;

import vn.ducquoc.sample.reflection.abstraction.ICatchPhrase;

/**
 * Created by ducquoc on 3/16/2018.
 */
public class Janitor extends Employee implements ICatchPhrase {

    //private info of a person (no Getter/Setter)
    private int waistMeasurement = 64;

    public String getFirstName() {
        return "Peace";
    }

    public String getLastName() {
        return "All Users";
    }

    public int getVacationDays() {
        return super.getVacationDays() - 1;
    }

    public String getCatchPhrase() {
        return "What is the catchphrase? Motto is too expensive, just slogan is enough. Tagline is cheaper.";
    }

    private String singASong() {

        String novemberRain = "When I look into your eyes\n" +
                "I can see a love restrained\n" +
                "But darlin' when I hold you\n" +
                "Don't you know I feel the same\n" +
                "Nothin' lasts forever\n" +
                "And we both know hearts can change\n" +
                "And it's hard to hold a candle\n" +
                "In the cold November rain\n" +
                "We've been through this such a long long time\n" +
                "Just tryin' to kill the pain, oo yeah\n" +
                "But love is always coming and love is always going\n" +
                "And no one's really sure who's lettin' go today\n" +
                "Walking away";

        System.out.println(novemberRain);

        return novemberRain;
    }

    private String translateASong() {

        String conMuaThangMuoi = "Nhìn tận sâu vào đôi mắt em \n" +
                "Chợt nhận thấy biết bao nỗi buồn \n" +
                "Này hỡi em yêu có thấu chăng \n" +
                "Những cảm giác trong anh cũng vậy \n" +
                "Không gì sẽ mãi trường tồn \n" +
                "Nên ta biết trái tim đổi thay \n" +
                "Và thật khó để giữ ánh lửa hồng \n" +
                "Trong giá buốt cơn mưa tháng Mười \n" +
                "Chúng ta từng đã phải sống chỉ để \n" +
                "chịu đựng những nỗi đau quá dài, oo yeah \n" +
                "Khi tình yêu vừa nơi đây khi tình yêu chợt bay xa \n" +
                "Và không một ai còn chắc trong 2 chúng ta còn đây \n" +
                "Sẽ cuốn đi";

        System.out.println(conMuaThangMuoi);

        return conMuaThangMuoi;
    }

}
