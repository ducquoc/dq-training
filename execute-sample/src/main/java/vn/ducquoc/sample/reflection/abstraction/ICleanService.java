package vn.ducquoc.sample.reflection.abstraction;

/**
 * Created by ducquoc on 3/16/2018.
 * <p></p>
 * C# convention: I... for interfaces
 */
public interface ICleanService {

    String cleanUp();

    String greenCleanUp();

    String quietlyCleanUp();

    String cleanUpAsAHobby();

}
