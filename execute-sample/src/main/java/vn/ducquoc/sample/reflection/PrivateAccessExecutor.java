package vn.ducquoc.sample.reflection;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * Created by ducquoc on 3/16/2018.
 * <br></br>
 * Inspired by Java 8 {@link java.lang.reflect.Executable}
 */
public class PrivateAccessExecutor {

    public static void main(String[] args) throws Exception {

        Employee employee = new Employee();
        Class employeeClass = employee.getClass();
        System.out.println("Employee: " + employee.getFirstName() + " " + employee.getLastName());

        Field protectedField = employeeClass.getDeclaredField("bodyMassIndex");
        protectedField.setAccessible(true);
        Integer protectedFieldValue = (Integer) protectedField.get(employee);
        System.out.println("\n protectedFieldValue = " + protectedFieldValue); // default value int = 0


        Janitor janitor = new Janitor();
        Class<?> janitorClass = janitor.getClass();
        System.out.println("Janitor: " + janitor.getFirstName() + " " + janitor.getLastName());

        Field privateField = janitorClass.getDeclaredField("waistMeasurement");
        privateField.setAccessible(true);
        Integer privateFieldValue = (Integer) privateField.get(janitor);
        System.out.println("\n privateFieldValue = " + privateFieldValue); // default value int = 0
        privateField.set(janitor, 70);
        System.out.println("\n new privateFieldValue = " + privateField.get(janitor) + " \n ");

        Method privateMethodEn = janitorClass.getDeclaredMethod("singASong");
        privateMethodEn.setAccessible(true);
        privateMethodEn.invoke(janitor, null);
        System.out.println("\n Invoked private method. \n ");

        Method privateMethodVn = janitorClass.getDeclaredMethod("translateASong");
        privateMethodVn.setAccessible(true);
        String retVal = (String) privateMethodVn.invoke(janitor, null);

        System.out.println("\n Invoked another private method. Result: \n \n" + retVal);
    }

}
