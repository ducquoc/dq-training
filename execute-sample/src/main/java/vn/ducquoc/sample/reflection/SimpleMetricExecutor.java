package vn.ducquoc.sample.reflection;

import vn.ducquoc.sample.reflection.abstraction.ICleanService;

import java.lang.reflect.Proxy;

/**
 * Created by ducquoc on 3/16/2018.
 * <br></br>
 * Inspired by Java 8 {@link java.lang.reflect.Executable}
 */
public class SimpleMetricExecutor {

    public static void main(String[] args) {

        ICleanService actualCleaningService = new CleanService();
        actualCleaningService.cleanUpAsAHobby();
        actualCleaningService.quietlyCleanUp();

        //JDK proxy - InvocationHandler (public method)
        ICleanService proxiedCleaningService = (ICleanService) Proxy.newProxyInstance(
                Thread.currentThread().getContextClassLoader(), new Class[]{ICleanService.class},
                new SimpleMetricInvocationHandler(actualCleaningService));

        proxiedCleaningService.cleanUp();
        proxiedCleaningService.quietlyCleanUp();
        proxiedCleaningService.greenCleanUp();
        proxiedCleaningService.cleanUpAsAHobby();

    }

}
