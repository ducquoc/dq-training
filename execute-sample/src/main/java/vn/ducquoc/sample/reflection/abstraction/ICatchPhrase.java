package vn.ducquoc.sample.reflection.abstraction;

/**
 * Created by ducquoc on 3/16/2018.
 * <p></p>
 * Catch-phrase-able, but here use C# convention: I... for interfaces
 */
public interface ICatchPhrase {

    // implicitly public static final, as public interface
    int TAGLINE = 100;
    int SLOGAN = 133;
    int MOTTO = 177;

    // implicitly public, as public interface
    String getCatchPhrase();

}
