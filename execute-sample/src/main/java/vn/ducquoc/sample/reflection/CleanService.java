package vn.ducquoc.sample.reflection;

import vn.ducquoc.sample.reflection.abstraction.ICleanService;
import vn.ducquoc.sample.reflection.annotation.DurationMetric;

/**
 * Created by ducquoc on 3/16/2018.
 */
public class CleanService implements ICleanService {


    @DurationMetric
    public String cleanUp() {
        quietlyPomodoro(1800);
        System.out.println("Cleaning is my job.");

        return "clean up";
    }

    @DurationMetric
    public String greenCleanUp() {
        quietlyPomodoro(1100);
        System.out.println("I like cleaning, and do it green for a better environment");
        quietlyPomodoro(1200);

        return "green clean up";
    }

    public String quietlyCleanUp() {
        quietlyPomodoro(1500);

        return "shadow";
    }

    @DurationMetric
    public String cleanUpAsAHobby() {
        quietlyPomodoro(2750);
        System.out.println("Cleaning is fun!");

        return "for fun and profit";
    }

    private void quietlyPomodoro(long timeInMilliseconds) {
        try {
            Thread.sleep(timeInMilliseconds);
        } catch (InterruptedException iEx) {
            System.out.println("can be ignored: " + iEx.getMessage());
        }
    }

}
