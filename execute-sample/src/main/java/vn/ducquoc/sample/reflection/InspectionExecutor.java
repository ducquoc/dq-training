package vn.ducquoc.sample.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

/**
 * Created by ducquoc on 3/22/2018.
 * <br></br>
 * Sample inspection by reflection API.
 */
public class InspectionExecutor {

    public static void main(String[] args) {

        Employee employee = new Employee();
        Class employeeClass = employee.getClass();
        System.out.println("Employee: " + employee.getFirstName() + " " + employee.getLastName());

        Field[] employeeFields = employeeClass.getDeclaredFields();
        for (Field field : employeeFields) {
            System.out.println("Field name: " + field.getName() + " - Type: " + field.getType() + " - Modifiers: " +
                    Modifier.toString(field.getModifiers()));
        }

        Method[] employeeMethods = employeeClass.getDeclaredMethods();
        for (Method method : employeeMethods) {
            System.out.println("Method name: " + method.getName() + " - ReturnType: " + method.getReturnType()
                    + " - Modifiers: " + Modifier.toString(method.getModifiers())
                    + " - ParamsTypes: " + classToString(method.getParameterTypes())
                    + " - ExceptionTypes: " + classToString(method.getExceptionTypes())
            );
        }

        Constructor[] employeeConstructors = employeeClass.getConstructors();
        for (Constructor ctor : employeeConstructors) {
            System.out.println("Name: " + ctor.getName() + " - Modifiers: " + Modifier.toString(ctor.getModifiers())
                    + " - ParamsTypes: " + classToString(ctor.getParameterTypes())
                    + " - ExceptionTypes: " + classToString(ctor.getExceptionTypes())
            );
        }

        Class[] employeeInterfaces = employeeClass.getInterfaces();
        System.out.println("Interfaces: " + classToStringJ8(employeeInterfaces));
    }

    public static String classToString(Class<?>... classes) {
        String SEPARATOR = ",";
        StringBuffer sb = new StringBuffer("");
        for (Class clazz : classes) {
            sb.append(clazz.getName()).append(SEPARATOR);
        }
        if (sb.length() > 0) {
            sb.deleteCharAt(sb.length() - 1);
        }
        return sb.toString();
    }

    public static String classToStringJ8(Class<?>... classes) {
        String SEPARATOR = ",";
        java.util.List<String> names = java.util.stream.Stream.of(classes).map(Class::getName).collect(java
                .util.stream.Collectors.toList());
        return String.join(SEPARATOR, names);
    }

}
