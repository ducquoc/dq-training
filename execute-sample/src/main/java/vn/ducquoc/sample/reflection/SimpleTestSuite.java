package vn.ducquoc.sample.reflection;

import vn.ducquoc.sample.reflection.annotation.UnitTest;

/**
 * Created by ducquoc on 3/16/2018.
 */
public class SimpleTestSuite {

    public void methodToCheck1() {
        System.out.println("methodToCheck1 - Regression");
    }

    @UnitTest
    public void methodToCheck2() {
        System.out.println("methodToCheck2 - Use-cases");
    }

    public void methodToCheck3() {
        System.out.println("methodToCheck3 - Main");
    }

    @UnitTest
    public void methodToCheck4() {
        System.out.println("methodToCheck4 - Other (explicit)");
    }

    @UnitTest
    public void methodToCheck5() {
        System.out.println("methodToCheck5 - Rare (implicit)");
    }

}
