package vn.ducquoc.sample.reflection;

import vn.ducquoc.sample.reflection.annotation.UnitTest;

import java.lang.reflect.Method;

/**
 * Created by ducquoc on 3/16/2018.
 * <br></br>
 * Inspired by Java 8 {@link java.lang.reflect.Executable}
 */
public class RunnerExecutor {

    public static void main(String[] args) {

        SimpleTestSuite suite = new SimpleTestSuite();
        Method[] methods = suite.getClass().getMethods();
        // as of Java 8, Method/Constructor is abstracted by Executable

        for (Method method : methods) {
            // no guaranteed order of the method with this suite executor/runner
            UnitTest annotatedUnitTest = method.getAnnotation(UnitTest.class);
            if (annotatedUnitTest != null) {
                try {
                    // will not check the "Main" and the "Regression", just "O" "R" "U"
                    method.invoke(suite);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    }

}
