package vn.ducquoc.sb;

import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import vn.ducquoc.sb.domain.User;
import vn.ducquoc.sb.domain.ProgramIdea;
import vn.ducquoc.sb.repository.UserRepository;
import vn.ducquoc.sb.repository.ProgramIdeaRepository;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class SbAuditTests {

  @Autowired
  private ProgramIdeaRepository programIdeaRepository;

  @Autowired
  private UserRepository userRepository;

  private User user;

  @Before
  public void create() {
    user = User.builder().name("ModifiedDate").username("modified.date").build();
    user = userRepository.save(user);

    assertThat(user.getCreated()).isNotNull();
    assertThat(user.getModified()).isNotNull();
  }

  @Test
  public void update() {
    LocalDateTime created = user.getCreated();
    LocalDateTime modified = user.getModified();

    User updated = User.builder().id(user.getId()).name(user.getName())
        .username("Modified").build();
    userRepository.save(updated);

    User updatedUser = userRepository.findById(user.getId()).orElse(null); //findOne(user.getId());

    assertThat(updatedUser.getUsername()).isEqualTo("Modified");

    assertThat(updatedUser.getCreated()).isEqualTo(created);

    assertThat(updatedUser.getModified()).isAfter(modified);//isGreaterThan
  }

  @Test
  public void should_update_programIdea_after_updating_lastSentDate_and_confirming_isSent() {

    ProgramIdea programIdea = new ProgramIdea();
    programIdea.setId(9999L);
    programIdea = programIdeaRepository.save(programIdea); //saveAndFlush(programIdea);

    log.info("A Parent updated: {}, child updated: {}", programIdea.parentUpdated, programIdea.childUpdated);
    if (programIdea.parentUpdated) {
      log.debug("A Parent: {}", programIdea.parentUpdated);
    }
    if (programIdea.childUpdated) {
      log.debug("A Child: {}", programIdea.childUpdated);
    }

    programIdea.setIsSent(true);
    programIdea.setLastSentDate(new java.util.Date());
    programIdea = programIdeaRepository.save(programIdea); //saveAndFlush(programIdea);

    log.info("U Parent updated: {}, child updated: {}", programIdea.parentUpdated, programIdea.childUpdated);
    if (programIdea.parentUpdated) {
      log.debug("U Parent: {}", programIdea.parentUpdated);
    }
    if (programIdea.childUpdated) {
      log.debug("U Child: {}", programIdea.childUpdated);
    }
    //org.junit.Assert.assertEquals(programIdea.getModifiedDate(), programIdea.getSecondUpdatedDate());

//    programIdeaRepository.delete(programIdea);
//
//    log.info("D Parent updated: {}, child updated: {}", programIdea.parentUpdated, programIdea.childUpdated);
//    if (programIdea.parentUpdated) {
//      log.debug("D Parent: {}", programIdea.parentUpdated);
//    }
//    if (programIdea.childUpdated) {
//      log.debug("D Child: {}", programIdea.childUpdated);
//    }

    //programIdeaRepository.flush();
  }

}
