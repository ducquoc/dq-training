package vn.ducquoc.sb.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import vn.ducquoc.sb.domain.User;

public interface UserRepository extends JpaRepository<User, Long> {
}
