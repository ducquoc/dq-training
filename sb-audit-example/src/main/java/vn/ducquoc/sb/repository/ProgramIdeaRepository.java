package vn.ducquoc.sb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import vn.ducquoc.sb.domain.ProgramIdea;

public interface ProgramIdeaRepository extends JpaRepository<ProgramIdea, Long> {



}
