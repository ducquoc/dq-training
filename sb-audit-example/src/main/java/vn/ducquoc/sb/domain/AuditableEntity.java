package vn.ducquoc.sb.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PostLoad;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Transient;
import java.util.Date;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@MappedSuperclass
public class AuditableEntity implements java.io.Serializable {

  @Id
  @GeneratedValue
  private Long id;

  @Column
  private Date createdDate;

  @Column
  private Date modifiedDate;

  @Transient
  public Boolean parentUpdated = Boolean.FALSE;

  @Transient
  private Date secondUpdatedDate;

  @PrePersist
  public void onCreated() {
    createdDate = new Date();
    modifiedDate = createdDate;
  }

  @PreUpdate
  public void onModified() {
    modifiedDate = new Date();
  }

  @PostLoad
  public void afterLoaded() {
    parentUpdated = true;
    secondUpdatedDate = modifiedDate;
  }

}
