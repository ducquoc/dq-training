package vn.ducquoc.sb.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Date;

@Slf4j
//@javax.persistence.ExcludeSuperclassListeners
@AllArgsConstructor
@NoArgsConstructor
@Data
@Table
@Entity
public class ProgramIdea extends AuditableEntity {

    //@Column
    private Boolean isSent = Boolean.FALSE;

    //@Column
    private Date lastSentDate;

    @Transient
    public Boolean childUpdated = Boolean.FALSE;

    @Override
    //@javax.persistence.PreUpdate
    public void onModified() {
        log.debug("Before child super: parentUpdated={}, childUpdated={}", parentUpdated, childUpdated);
        //super.onModified(); //might not need to execute parent method
        log.debug("After child super: parentUpdated={}, childUpdated={}", parentUpdated, childUpdated);
        Date twoWDate = getSecondUpdatedDate();
        if (twoWDate != null && twoWDate.before(getModifiedDate())) { //both date should be null-safe
            setModifiedDate(twoWDate);
        }
        log.debug("After child PreUp: parentUpdated={}, modified={}, 2W={}", parentUpdated, getModifiedDate(), twoWDate);
    }

    public Boolean getSent() {
        return isSent;
    }

    public void setSent(Boolean sent) {
        isSent = sent;
        childUpdated = true;
        log.debug("Setting value isSent={}, childUpdated={}", isSent, childUpdated); //debug lv after tests
    }

    public Date getLastSentDate() {
        return lastSentDate;
    }

    public void setLastSentDate(Date lastSentDate) {
        this.lastSentDate = lastSentDate;
        childUpdated = true;
        log.debug("Setting value lastSentDate={}, childUpdated={}", lastSentDate, childUpdated); //debug lv after tests
    }

}
