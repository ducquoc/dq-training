package vn.ducquoc.sb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SbAuditApplication {

    public static void main(String[] args) {
        SpringApplication.run(SbAuditApplication.class, args);
    }
}
