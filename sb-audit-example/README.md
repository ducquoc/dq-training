=== Spring Boot audit example

==== Rationale

Just a sample to verify/validate some thoughts about Spring Data - via Spring Boot.

==== How to use

Build: 

```
mvn clean install
```
or using maven wrapper
```
mvnw clean install
```

Run: 

```
mvn spring-boot:run
```

```
mvn test -Dspring.profiles.active=local
```

Just package:

```
mvn package spring-boot:repackage
```
